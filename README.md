# Bem-vindx Aventureirx!

Em uma taverna, 5 amigos se encontram sentados em uma mesa. Estes são os heróis dessa história. 

Inspirados por músicas e vários canecos de cerveja de barril, nossos aventureiros relembram momentos que passaram juntos, amigos que perderam e experiências que ganharam ao longo de suas quests.

Após algumas horas na taverna, os 5 heróis são surpreendidos por um estranho que os oferece recompensas incríveis em troca de alguns serviços, que de acordo com ele apenas estes heróis em todo o Reino são capazes de realizar.

Os 5 amigos, sedentos por uma nova aventura (por mais arriscada que possa ser) topam de imediato.

Para esta quest, os heróis mapearam que um novo aventureiro deverá se juntar ao time, alguém com experiência, de confiança e que seja crucial para o sucesso da jornada.

Se você está recebendo este desafio, nossos heróis contam com você para se juntar a eles e participar das mais incríveis jornadas em todo o Reino.

Se você encontrou este desafio, bem vindx. Talvez você é o herói da profecia que todo o Reino está esperando se concretizar.

## Especificações do desafio

Nesse desafio você deverá fazer uma plataforma web para nosso mestre controlar as quests e heróis do Reino. Portanto, deverás seguir [este design](https://xd.adobe.com/spec/f89c9446-3b91-4c7b-750c-6702c132a8e8-8ead/), onde você encontrará todos os elementos (imagens, nomes de fontes e fluxo de navegação) necessários para construção de sua plataforma.

Teremos duas abas para Heróis e Quests, onde em cada uma será possível cadastrar cada uma das informações e correlacionar cada quests com os heróis já cadastrados.

### Heróis

Na aba `Overall` será mostrado cada herói com sua experiência (pode colocar qualquer valor) e suas quests em andamento.

Na aba `Heróis` são listados todos os heróis da plataforma.

Na aba `Adicionar herói` é onde nosso querido mestre adiciona seus heróis com seu respectivo nome e especialidade.

### Quests

Na aba `Overall` será mostrado cada quest com seu nome, inicio e fim estimado e os heróis participantes dessa quest. Além de um seletor de `status` da quest que terá as opções de `Em andamento`, `Concluída no dia`, `Antecipada` e `Atrasada`.

Na aba `Criar quest` é onde nosso querido mestre cria as quests do Reino, cadastrando seu nome, data de início, entrega estimada e entrega máxima e os heróis da quest.

## Observações

- Você não precisa se preocupar com o cálculo da experiência, portanto, pode deixá-la estática ou caso queira criar um cálculo sinta-se à vontade.

- O desafio pode ser feito com qualquer framework/lib (React, Angular, Vue, Svelte...) `JavaScript` que você preferir, caso queira fazer com `JavaScript` puro, sinta-se à vontade. Será valorizado o uso de `TypeScript`, quando possível. Entendemos que no caso do `Svelte`, por exemplo, ainda não possui suporte para `TypeScript`.

- Você não tem data fim para o desafio. Portanto, para fazê-lo basta fazer um `fork` deste repositório e abrir um `merge request` quando finalizar o desafio.

- No seu repositório deve conter no `README.md` o tempo que vc gastou para completar o desafio e as instruções para rodar o projeto.

- Será valorizado a persistência dos dados. Portanto, será de muito bom grado para nós se você conseguir persistir os dados no `local storage` do navegador ou, caso utilize outra forma de persistência (como `Firebase`), também será válido.

## Referências

* [git - the simple guide](https://rogerdudler.github.io/git-guide/)
* [How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/)
* [Mostly Adequate Guide to Functional Programming](https://github.com/MostlyAdequate/mostly-adequate-guide)
* [BEM - Block Element Modifier](http://getbem.com/introduction/)